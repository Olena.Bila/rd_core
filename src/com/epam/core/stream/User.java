package com.epam.core.stream;

import java.util.Collection;

public class User implements Comparable<User> {
    private Long id;
    private String firstName;
    private String lastName;
    private int age;


    private Collection<String> roles;

    public User(Long id, String firstName, String lastName, int age, Collection<String> roles) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Collection<String> getRoles() {
        return roles;
    }

    public void setRoles(Collection<String> roles) {
        this.roles = roles;
    }

    @Override
    public String toString(){
        return firstName + " " + lastName;
    }

    @Override
    public int compareTo(User o) {
        int last = lastName.compareTo(o.lastName);
        return last != 0 ? last : firstName.compareTo(firstName);
    }

}
