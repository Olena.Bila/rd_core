package com.epam.core.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamCreatingDemo{
    public static void main(String[] args) throws IOException {

        Collection<String> collection = Arrays.asList("creating", "com/epam/core/stream", "from", "com/epam/core/collection");
        Stream<String> words = collection.stream();
        show("from collection", words);

        Stream<String> counts = Stream.of("one", "two", "three", "four");
        show("words", counts);

        Stream<String> silence = Stream.empty();
        show("silence", silence);

        Stream<String> echos = Stream.generate(() -> "Echo");
        show("echos", echos);

        Stream<Double> randoms = Stream.generate(Math::random);
        show("randoms", randoms);

        Stream<String> lines = Files.lines(Paths.get("alice_in_wonderland.txt"));
        show("From file", lines);
    }

    private static <T> void show(String title, Stream<T> stream) {
        final int size = 10;
        List<T> firstElements = stream
                .limit(size + 1)
                .collect(Collectors.toList());

        System.out.print(title + ": ");
        for (int i = 0; i < firstElements.size(); i++) {
            if (i > 0) System.out.print(", ");
            if (i < size) System.out.print(firstElements.get(i));
            else System.out.print("...");
        }
        System.out.println();
    }
}
