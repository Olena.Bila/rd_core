package com.epam.core.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class WorldCounter {
    public static void main(String[] args) throws IOException {
        String contents = new String(Files.readAllBytes(Paths.get("alice_in_wonderland.txt")));

        List<String> words = Arrays.asList(contents.split("\\PL+"));
        long count = 0;
        for (String w : words) {
            if (w.length() > 12) count++;
        }
        System.out.println(count);

        long count1 = words.stream().filter(w -> w.length() > 12).count();
        System.out.println(count1);
    }
}
