package com.epam.core.stream;

import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CollectingDemo {

    public static void main(String[] args) throws IOException {
        Collection<User> people = TransformationDemo.generatePeople();

        Long count = people.stream().count();
        System.out.println("Number of people: " + count);

        //max method works the same
        User minUser = people.stream()
                .min(Comparator.comparing(User::getLastName))
                .get();
        System.out.println("Min person by last name: " + minUser);

        //findAny returns first user
        Optional<User> first = people.stream().findFirst();
        System.out.println("First person: " + first);

        //allMatch returns true if all elements satisfy the condition
        //anyMatch returns true if any element satisfies the condition
        //not returns true if any element satisfies the condition
        boolean allMatch = people.stream()
                .allMatch(user -> user.getAge() > 10);
        System.out.println("All users are older the 10: " + allMatch);

        //Collecting to list
        List<User> user = people.stream().collect(Collectors.toList());

        //Collecting to Set
        Set<User> uniqueUsers = people.stream().collect(Collectors.toSet());

        //Collection to TreeSet
        Set<User> treeSet = people.stream()
                .collect(Collectors.toCollection(TreeSet::new));

        //Collecting to Map
        Map<Long, User> userToId = people.stream()
                .collect(Collectors.toMap(User::getId, Function.identity()));

        //Specify strategy for keys collision - take first value
        Map<Long, User> userToId1 = people.stream()
                .collect(Collectors.toMap(User::getId, Function.identity(), (v1, v2) -> v1));

        //Specify strategy for keys collision - merge values (take the first name of first user and last name and age of the second user)
        Map<Long, User> userToId2 = people.stream()
                .collect(Collectors.toMap(User::getId, Function.identity(),
                        (v1, v2) ->
                                new User(v1.getId(), v1.getFirstName(), v2.getLastName(), v2.getAge(), null)));


        Map<Long, String> lastNameToId = people.stream()
                .collect(Collectors.toMap(User::getId, User::getLastName));

        //groupBy
        Map<Integer, List<User>> usersByAge = people.stream()
                .collect(Collectors.groupingBy(User::getAge));

        //Chose type of collection for values
        Map<Integer, Set<User>> usersByAgeSet = people.stream()
                .collect(Collectors.groupingBy(User::getAge,
                        Collectors.toCollection(TreeSet::new)));

        //groupBy with counting
        Map<Integer, Long> usersByAgeCountion = people.stream()
                .collect(Collectors.groupingBy(User::getAge, Collectors.counting()));

        //partitionBy
        Map<Boolean, List<User>> youngToOld = people.stream()
                .collect(Collectors.partitioningBy(u -> u.getAge() < 40));
    }
}
