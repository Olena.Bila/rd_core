package com.epam.core.stream;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TransformationDemo {
    public static void main(String[] args) {
        Collection<User> people = generatePeople();

        //Filter
        long young = people.stream().filter(user -> user.getAge() < 40).count();
        System.out.println("Young people: " + young);

        //map
        Pair<String> firstPerson = people.stream().
                map(user -> new Pair<>(user.getFirstName(), user.getLastName()))
                .findFirst().get();
        System.out.println("First name: " + firstPerson.getFirst() + ", Last name: " + firstPerson.getSecond());

        //flatMap
        Set<String> uniqueRoles = people.stream()
                .flatMap(person -> person.getRoles().stream())
                .collect(Collectors.toSet());
        uniqueRoles.forEach(System.out::println);

        //distinct
        Collection<String> uniqueWords = Stream.of("merrily", "merrily", "merrily", "gently")
                .distinct()
                .collect(Collectors.toList());
        System.out.println(uniqueWords);

        //sort
        List<User> sorted = people.stream()
                .sorted(Comparator.comparing(User::getAge))
                .collect(Collectors.toList());
        System.out.println(sorted);
    }

    public static Collection<User> generatePeople(){
        User user1 = new User(1L, "John", "Doe", 20, Collections.EMPTY_LIST);
        User user2 = new User(2L, "Jane", "Doe", 18, Collections.EMPTY_LIST);
        User user3 = new User(3L, "John", "Smith", 60, Arrays.asList("admin", "role1"));
        User user4 = new User(4L, "Garry", "Jones", 20, Arrays.asList("admin", "role2"));
        User user5 = new User(5L, "Carry", "Williams", 20,  Arrays.asList("role3"));
        User user6 = new User(6L, "Geoff", "King", 42,  Arrays.asList("role1", "role2"));
        User user7 = new User(7L, "Cris", "Yong", 18, Collections.EMPTY_LIST);
        return Arrays.asList(user1, user2, user3, user4, user5, user6, user7);
    }
}
