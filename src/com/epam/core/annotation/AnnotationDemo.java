package com.epam.core.annotation;

//@BugReport(assignTo = "anonymous", severity = 1)
// can't put @BugReport here because annotation doesn't have @Target CLASS
public class AnnotationDemo {
    //severity = 5, was set to default value
    @BugReport(assignTo = "Homer Simpson")
    private String field;

    @BugReport(assignTo = "John Doe", severity = 1)
    public void method(){

    }
}
