package com.epam.core.annotation;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class BugReportProcessorDemo {
    public static void main(String[] args) throws ClassNotFoundException {
        Class reportDemoClass = Class.forName("com.epam.core.annotation.AnnotationDemo");
        for (Method m : reportDemoClass.getMethods()) {
            if (m.isAnnotationPresent(BugReport.class)) {
                BugReport annotation = m.getAnnotation(BugReport.class);
                System.out.println(String.format("Bug Report info: severity - %d, assignedTo - %s",
                        annotation.severity(), annotation.assignTo()));
            }
        }

        for (Field f : reportDemoClass.getDeclaredFields()) {
            if (f.isAnnotationPresent(BugReport.class)) {
                BugReport annotation = f.getAnnotation(BugReport.class);
                System.out.println(String.format("Bug Report info: severity - %d, assignedTo - %s",
                        annotation.severity(), annotation.assignTo()));
            }
        }
    }
}
