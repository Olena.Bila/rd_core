package com.epam.core.collection;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetDemo {
    public static void main(String[] args) {
        Set<String> uniqueWords = new HashSet<>();
        uniqueWords.add("one");
        uniqueWords.add("two");
        uniqueWords.add("three");
        boolean added = uniqueWords.add("one");
        uniqueWords.add("four");

        System.out.println(uniqueWords);
        System.out.println(added);

        TreeSet<Person> people = new TreeSet<>();
        people.add(new Person("Jane", "Doe"));
        people.add(new Person("Homer", "Simpson"));
        people.add(new Person("John", "Smith"));
        people.add(new Person("Marge", "Simpson"));
        people.add(new Person("James", "Bond"));
        System.out.println(people);
//
        Comparator<Person> compareByFirstName = Comparator.comparing(Person::getLastName);
        TreeSet<Person> people1 = new TreeSet<>(compareByFirstName);
        people1.add(new Person("Jane", "Doe"));
        people1.add(new Person("Homer", "Simpson"));
        people1.add(new Person("John", "Smith"));
        people1.add(new Person("Marge", "Simpson"));
        people1.add(new Person("James", "Bond"));
        people1.forEach(System.out::println);
    }
}
