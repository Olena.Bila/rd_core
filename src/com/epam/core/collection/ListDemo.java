package com.epam.core.collection;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {
    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        words.add("one");
        words.add("two");
        words.add("three");
        boolean added = words.add("one");
        words.add("four");
        words.add("five");

        //add not unique element
        System.out.println(added);
        //elements are ordered
        System.out.println(words);

        //add element in particular position
        words.add(3, "added");
//        System.out.println(words);
//
//        //set element in particular position
        words.set(3, "replaced");
        System.out.println(words);
//
//        //get element by index
//        String world = words.get(3);
//        System.out.println(world);
//
//        //find out element index
        int index = words.indexOf("replaced");
        System.out.println(index);
//
        words.remove(3);
        System.out.println(words);
        boolean oneRemoved = words.remove("one");
        System.out.println(words);
    }
}
