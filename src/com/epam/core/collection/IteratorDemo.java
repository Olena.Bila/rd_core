package com.epam.core.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class IteratorDemo {
    public static void main(String[] args) {
        //create a collection
        Collection<String> words = new ArrayList<String>();
        words.add("one");
        words.add("two");
        words.add("three");
        words.add("four");
        words.add("five");

        Iterator<String> iterator = words.iterator();

        //3 styles to iterate the collection
        while (iterator.hasNext()) {
            String element = iterator.next();
            System.out.println(element);
        }

        for (String word : words) {
            System.out.println(word);
        }

        iterator.forEachRemaining(e -> System.out.println(e));
        Iterator<String> iterator1 = words.iterator();
        iterator1.forEachRemaining(System.out::println);

        //removeElement(words);
        //twoIterators(words);
        //illegalRemove(words);
    }

    public static void removeElement(Collection<String> collection) {
        Iterator<String> iterator = collection.iterator();
        while (iterator.hasNext()) {
            String element = iterator.next();
            if (element.equals("three")) {
                iterator.remove();
                System.out.println("Element " + element + " was removed");
            }
        }
    }

    public static void illegalRemove(Collection<String> collection) {
        Iterator<String> iterator = collection.iterator();
        while (iterator.hasNext()) {
            String element = iterator.next();
            iterator.remove();
            iterator.next();
            iterator.remove();
        }
    }

    //in case of several iterators you are not allowed to change collection
    public static void twoIterators(Collection<String> collection) {
        Iterator<String> iter1 = collection.iterator();
        Iterator<String> iter2 = collection.iterator();
        iter1.next();
        iter1.remove();
        iter2.next();
    }
}
