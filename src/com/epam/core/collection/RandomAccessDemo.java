package com.epam.core.collection;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class RandomAccessDemo {
    public static void main(String[] args) {
        //create and initialize linked list
        List<Integer> linkedList = new LinkedList<>();
        for(int i = 0; i < 100000; i++){
            linkedList.add(i);
        }

        long start = System.nanoTime();
        int sum = 0;
        //random access
        for (int i = 0; i < 100000; i++){
            sum = sum + linkedList.get(i);
        }
        long finish = System.nanoTime();

        System.out.println("linked:" + sum);
        System.out.println("linked:" + (finish - start));


        //create and initialize array list
        List<Integer> arrayList = new ArrayList<>();
        for(int i = 0; i < 100000; i++){
            arrayList.add(i);
        }

        long start1 = System.nanoTime();
        int sum1 = 0;
        //random access
        for (int i = 0; i < 100000; i++){
            sum1 = sum1 + arrayList.get(i);
        }
        long finish1 = System.nanoTime();

        System.out.println("array:" + sum1);
        System.out.println("array:" +  (finish1- start1));

        System.out.println("Array list is faster in " + (finish - start)/(finish1 - start1) + " times");
    }
}
