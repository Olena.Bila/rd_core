package com.epam.core.collection;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapDemo {
    public static void main(String[] args) {
        Map<Integer, Person> people = new HashMap<>();
        people.put(1, new Person("John", "Doe"));
        people.put(2, new Person("Jane", "Doe"));
        people.put(3, new Person("James", "Bond"));

        //iterate though the map
        //people.forEach((k, v) -> System.out.println("Key:" + k + " Value:" + v));

        //removing an element
//        people.remove(2);
//        people.forEach((k, v) -> System.out.println("Key:" + k + " Value:" + v));
//
//        //put element with the same key
//        people.put(3, new Person("Jack", "Richer"));
//
//        people.forEach((k, v) -> System.out.println("Key:" + k + " Value:" + v));

        //Map views
        Set<Integer> keys = people.keySet();
        keys.remove(2);
        keys.add(4);
        //System.out.println(keys.size());

        Collection<Person> values = people.values();
        //values.add(new Person("", ""));
        System.out.println(values);

        Set<Map.Entry<Integer, Person>> entries = people.entrySet();
        entries.forEach(entry -> {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        });
        
        for(Map.Entry<Integer, Person> entry : entries){
            entry.getKey();
            entry.getValue();
        }
    }
}
