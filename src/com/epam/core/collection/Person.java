package com.epam.core.collection;

public class Person implements Comparable<Person> {
    private String firstName;
    private String lastName;

    public Person(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public int compareTo(Person o) {
        int last = lastName.compareTo(o.lastName);
        return last != 0 ? last : firstName.compareTo(o.firstName);
    }

    @Override
    public String toString(){
        return firstName + " " + lastName;
    }

}
