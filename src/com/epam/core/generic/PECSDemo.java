package com.epam.core.generic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PECSDemo {
    public static void main(String[] args) {
        List<Integer> ints = new ArrayList<>();
        ints.add(42);
        processElements(ints);

        List<Double> doubles = new ArrayList<>();
        doubles.add(49.99);
        processElements(doubles);
    }

    //since we only read elements from collection we use "? extends " (collection is producer)
    private static void processElements(List<? extends Number> numbers) {
        for (Number n : numbers) {
            //do something with elements
        }

        //compilation error - can't add anything here (compiler don't know what type of elements are in list)
        //numbers.add(89);
    }

    //since we only add elements to collection we use "? super "
    private static void consumeElements(List<? super Number> numbers) {
        numbers.add(42);
        numbers.add(59.99);
        numbers.add(BigDecimal.valueOf(15.00));
    }
}
