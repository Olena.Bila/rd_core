package com.epam.core.generic;

public class GenericDemo {
    public static void main(String[] args) {
        Person he = new Person("John", "Doe");
        Person she = new Person("Jane", "Doe");

        //generic class creation
        Pair<Person> couple = new Pair<>(he, she);

        Person john = couple.getFirst();
        System.out.println(john.getFirstName());
    }
}
