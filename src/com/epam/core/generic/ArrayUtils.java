package com.epam.core.generic;

public class ArrayUtils {
    public static void main(String[] args) {
        Person[] people = new Person[] {
                new Person("John", "Doe"),
                new Person("Homer", "Simpson"),
                new Person("Jane", "Doe")
        };

        //using generic view for different types of parameters
        Person middle = getMiddleElement(people);
        String middleString = getMiddleElement("one", "two", "three", "four", "five");

        //type variables bounds
        Person min = getMinimum(people);
        System.out.println(min.getFirstName());
    }

    //Generic method
    public static <T> T getMiddleElement(T... array){
        return array[array.length/2];
    }

    //Generic method with boundary type parameter
    public static <T extends Comparable<T>> T getMinimum(T[] array){
        T smallest = array[0];
        for(int i = 0; i < array.length; i++){
            if(smallest.compareTo(array[i]) > 0){
                smallest = array[i];
            }
        }
        return smallest;
    }
}
