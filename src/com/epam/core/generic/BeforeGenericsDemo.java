package com.epam.core.generic;

import java.util.ArrayList;
import java.util.List;

public class BeforeGenericsDemo {
    public static void main(String[] args) {
        List oldList = new ArrayList();
        oldList.add("String");
        oldList.add("Other String");

        //have to add explicit cast
        String element = (String) oldList.get(1);
        System.out.println(element);

        //easy to break list by adding some unexpected element
        //any errors here
        oldList.add(new Person("John", "Doe"));

        //ClassCastException
        String element1 = (String) oldList.get(2);


    }
}
