package com.epam.core.exception;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.EmptyStackException;
import java.util.Stack;
import java.util.concurrent.TimeoutException;

public class ExceptionDemo {
    public static void main(String[] args) {
        //an example with exceptions in overridden methods
        Parent parent = new Child();
        try {
            parent.m();
        } catch (IOException ex) {
            //handle exception
        }
    }

    public static void foo() throws IOException {
        throw new FileNotFoundException();
    }

    public static void other() throws TimeoutException {
        throw new TimeoutException();
    }

    //try/catch
    public static void bar() {
        try {
            foo();
            other();
        }

        //since Java 7
        catch (IOException | TimeoutException ex) {
            //re-throwing exception
            throw new BusinessException();
        }
//      wrong place for FileNotFoundException catch
//      catch (FileNotFoundException ex){
//      }
        finally {
            //put code that should be executed in any case here
        }
    }
}
