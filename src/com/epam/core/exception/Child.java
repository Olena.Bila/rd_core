package com.epam.core.exception;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Child extends Parent {
    @Override
    //can throw only IOException and it's subtypes
    public void m() throws FileNotFoundException {}
}
