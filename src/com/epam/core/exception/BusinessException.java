package com.epam.core.exception;

//an example of custom exception
public class BusinessException extends RuntimeException {
    public BusinessException(){
        super();
    }

    public BusinessException(String message){
        super(message);
    }
}
