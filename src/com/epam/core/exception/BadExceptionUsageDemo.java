package com.epam.core.exception;

import java.util.EmptyStackException;
import java.util.Stack;

public class BadExceptionUsageDemo {
    //don't use exception mechanism instead of simple check
    //use s.empty() check here
    public void badExeptionUsage() {
        Stack<String> s = new Stack<>();
        s.push("Hi");
        try {
            s.pop();
        } catch (EmptyStackException e) {
        }
    }
}
