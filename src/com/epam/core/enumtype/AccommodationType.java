package com.epam.core.enumtype;

public enum AccommodationType {
    HOTEL("HT"), HOSTEL("HS"), APARTMENT("AP"), VILLA("VL");

    private String abbr;
    AccommodationType(String abbr) {
        this.abbr = abbr;
    }

    public String getAbbr(){
        return abbr;
    }
}
