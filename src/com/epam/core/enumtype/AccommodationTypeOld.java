package com.epam.core.enumtype;

//Enum pattern, was used before java 1.5
public class AccommodationTypeOld {
    private AccommodationTypeOld(){}

    public static AccommodationTypeOld HOTEL = new AccommodationTypeOld();
    public static AccommodationTypeOld HOSTEL = new AccommodationTypeOld();
    public static AccommodationTypeOld APPARTMENT = new AccommodationTypeOld();
}
