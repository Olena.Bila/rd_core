package com.epam.core.enumtype;

import java.util.stream.Stream;

public class EnumDemo {

    public static void main(String[] args) {
        //create enum from string
        AccommodationType villa = Enum.valueOf(AccommodationType.class, "VILLA");
        System.out.println("Hotel = " + villa);

        AccommodationType apartment = AccommodationType.valueOf("APARTMENT");
        System.out.println("Apartment = " + apartment);

        //values() view
        AccommodationType[] allTypes = AccommodationType.values();
        System.out.println("All hotel types: ");
        Stream.of(allTypes).forEach(System.out::println);
        System.out.println();

        //ordinal view, ordinal position of enumeration constant in enum declaration
        int ordinal = villa.ordinal();
        System.out.println("Villa's ordinal = " + ordinal);

        //toString view
        String villaString = villa.toString();
        System.out.println("To string: " + villaString);

        //equality of enums, hostel and hostel1 references are pointed to the same instance
        AccommodationType hostel = AccommodationType.HOSTEL;
        AccommodationType hostel1 = AccommodationType.HOSTEL;
        System.out.println(hostel1 == hostel);

        //compareTo view, enum constants are compared by ordinal value
        if(villa.compareTo(hostel) > 0) {
            System.out.println("Villa is bigger that hostel");
        } else {
            System.out.println("Hostel is bigger that villa");
        }
    }
}
