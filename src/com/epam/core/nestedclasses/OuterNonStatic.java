package com.epam.core.nestedclasses;

public class OuterNonStatic {

    private int value;

    private void method(){
        value++;
    }

    class Inner {
        //Inner class has a reference to outer class

        public void view() {
            //Inner class has access to outer's fields and methods
            System.out.println("Hello from inner class! Value = " + OuterNonStatic.this.value);
            OuterNonStatic.this.method();
            System.out.println("Hello from inner class! Value = " + OuterNonStatic.this.value);
        }
    }

    public static void main(String[] args) {
        OuterNonStatic outer = new OuterNonStatic();
        OuterNonStatic.Inner inner = outer.new Inner();
        inner.view();
    }
}
