package com.epam.core.nestedclasses;

public class InnerLocal {
    private int value;

    public void foo(int param) {
        //param must be final or effectively final
        //param++; can't change param value
        class LocalClass {
            //local class has access to outer's class fields and methods and to method params
            public int getValue() {
                return param + value;
            }
        }
    }
}
