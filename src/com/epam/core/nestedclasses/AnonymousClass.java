package com.epam.core.nestedclasses;

import java.util.ArrayList;
import java.util.List;

public class AnonymousClass {

    //example of anonymous class using lambda
    Runnable runnable = () -> {
        //some code
    };

    public static void main(String[] args) {
        //example of anonymous class
        List<String> list = new ArrayList<String>() {
            @Override
            public int size() {
                return 10;
            }
        };
        System.out.println(list.size());
    }
}
