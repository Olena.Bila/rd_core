package com.epam.core.nestedclasses;

public class OuterStatic {
    private int value;

    private void method() {
        value++;
    }

    static class NestedStatic {
        //nested class doesn't have access to outer's non static fields and methods
        public void hello() {
            //OuterStatic.this.method(); - compilation error
            System.out.println("Hello from Inner");
        }
    }

    public static void main(String[] args) {
        OuterStatic.NestedStatic nested = new OuterStatic.NestedStatic();
        nested.hello();
    }
}
